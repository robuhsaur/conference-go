import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    bruh = json.loads(body)
    send_mail(
        "Your presentation has been accepted",
        f"{bruh['presenter_name']}, we're happy to tell you that your presentation {bruh['title']} has been accepted",
        "admin@conference.go",
        [bruh["presenter_email"]],
        fail_silently=False,
    )
    print("  Received %r" % body)


def process_rejection(ch, method, properties, body):
    bruh = json.loads(body)
    send_mail(
        "Your presentation has been rejected",
        f"{bruh['presenter_name']}, we're sorry to tell you that your presentation {bruh['title']} has been rejected",
        "admin@conference.go",
        [bruh["presenter_email"]],
        fail_silently=False,
    )
    print("  Received %r" % body)


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )

        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
