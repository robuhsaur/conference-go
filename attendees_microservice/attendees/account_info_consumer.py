import json
import pika
import django
import os
import sys
from pika.exceptions import AMQPConnectionError
import time
from datetime import datetime


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()


from attendees.models import AccountVO


# Declare a function to update the AccountVO object (ch, method, properties, body)
def update_AccoutVO(ch, method, properties, body):
    #   content = load the json in body
    content = json.loads(body)
    #   first_name = content["first_name"]
    first_name = content["first_name"]
    #   last_name = content["last_name"]
    last_name = content["last_name"]
    #   email = content["email"]
    email = content["email"]
    #   is_active = content["is_active"]
    is_active = content["is_active"]
    #   updated_string = content["updated"]
    updated_string = content["updated"]
    #   updated = convert updated_string from ISO string to datetime
    updated = datetime.fromisoformat(updated_string)
    if is_active:
        AccountVO.objects.update_or_create(
            {
                "first_name": first_name,
                "last_name": last_name,
                "email": email,
                "is_active": is_active,
                "updated": updated,
            }
        )
        # Use the update_or_create method of the AccountVO.objects QuerySet
    #           to update or create the AccountVO object
    else:
        # Delete the AccountVO object with the specified email, if it exists
        account = AccountVO.objects.filter(email=email)
        AccountVO.objects.delete(account)


while True:
    try:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host="rabbitmq")
        )
        channel = connection.channel()

        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )

        result = channel.queue_declare(queue="", exclusive=True)
        queue_name = result.method.queue

        channel.queue_bind(exchange="account_info", queue=queue_name)

        print(" [*] Waiting for logs. To exit press CTRL+C")
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_AccoutVO,
            auto_ack=True,
        )

        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
